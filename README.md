Macaw Beamer Template
---------------------

This is the official FLUSP LaTeX Beamer Template for slides.

Files are:

- `beamercolorthememacaw.sty`: Colors
- `beamerinnerthememacaw.sty`: Inner
- `beamerouterthememacaw.sty`: Outer
- `beamerthememacaw.sty`: Structure
- `macaw-example.tex`: Sample slides
