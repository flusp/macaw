engine = pdflatex
file = macaw-example
compile = $(engine) $(file).tex

all:
	make clean; \
	$(compile); $(compile); \
 	biber $(file); \
 	$(compile); $(compile) && \
	echo "Moving $(file).pdf to sample/$(file).pdf" && \
	mv $(file).pdf sample/ && \
 	zathura sample/$(file).pdf

.PHONY: clean
clean:
	rm -f *.aux *.bbl *.bcf *.blg *.log *.out *.run.xml *.toc *.nav *.snm *.synctex.gz-current;
	rm -f *.synctex.gz *_vimtex.* *.fls *.fdb_latexmk *.pdf;
	rm -rf _minted-report

